
(function () {

  var inputSearchPlace = document.querySelectorAll('.search-place');
  var resultAutocomplete = document.querySelectorAll('.result-autocomplete');

  function $on(element, event, callback) {
    for (var i = 0; i < element.length; i++) {
      element[i].addEventListener(event, callback);
    }
  }

  $on(inputSearchPlace, 'focus', selectValue);
  $on(inputSearchPlace, 'keyup', autoComplete);
  $on(resultAutocomplete, 'click', selectPlaceByClick);

  var timerForRequest;

  function autoComplete(e) {
    var term = e.target.value;
    var airports;

    clearTimeout(timerForRequest);

    if (term !== '') {

      var x = e.which || e.keyCode;

      if (x === 40) {
        selectDown(e);
      } else if (x === 38) {
        selectUp(e);
      } else if (x === 13) {
        selectConfirm(e);
      } else {
        timerForRequest = setTimeout(function () {
          fetchData(term).then(result => {
            if (result.airports.length > 0) {
              createList(result.airports, e.target);
            }
          }).catch(error => {
          });
        }, 150);
      }
    } else {
      clearList(e.target);
    }
  }

  function fetchData(term) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest()
      xhr.open('GET', 'https://api.fly.com/MegaSearch/services/v1/airports/searchAirports?query=' + term)
      xhr.onload = function () {
        var result = JSON.parse(xhr.responseText)
        if (result) {
          resolve(result);
        } else {
          reject();
        }
      }
      xhr.send()
    });
  }

  function createList(airports, element) {
    let airportsLabels = airports.map(item => ({
      flag: item.flag,
      label: `${(item.cityName) ? item.cityName + ', ' : ''} ${(item.countryName) ? item.countryName + ' -' : ''} ${item.airportName}(${item.airportCode})`
    }));

    let list = `<ul class="list-unstyled">`;
    for (var i = 0; i < airportsLabels.length; i++) {
      list += `<li data-value="${airportsLabels[i].label}">
        <img src="${airportsLabels[i].flag}">
        ${airportsLabels[i].label}
      </li>`;
    }
    list += `</ul>`;

    element.parentNode.querySelector('.result-autocomplete').innerHTML = list;
  }

  function clearList(element) {
    element.parentNode.querySelector('.result-autocomplete').innerHTML = '';
  }

  function selectValue(e) {
    e.target.select();
    if (e.target.value === '') {
      clearList(e.target);
    }
  }

  function selectPlaceByClick(e) {
    var value = e.target.dataset.value;
    var formGroup = e.target.closest('.form-group')
    formGroup.querySelector('input').value = value;
    e.target.closest('.result-autocomplete').innerHTML = '';
  }

  function selectDown(e) {
    var formGroup = e.target.closest('.form-group')
    var list = formGroup.querySelector('.result-autocomplete');
    var active = list.querySelector('ul li.active');
    if (!active) {
      active = list.querySelector('ul li:first-child');
      active.classList.add('active');
    } else {
      if (active.nextElementSibling) {
        active.classList.remove('active');
        active.nextElementSibling.classList.add('active');
      }
    }
  }

  function selectUp(e) {
    var formGroup = e.target.closest('.form-group')
    var list = formGroup.querySelector('.result-autocomplete');
    var active = list.querySelector('ul li.active');
    if (!active) {
      active = list.querySelector('ul li:last-child');
      active.classList.add('active');
    } else {
      if (active.previousElementSibling) {
        active.classList.remove('active');
        active.previousElementSibling.classList.add('active');
      }
    }
  }

  function selectConfirm(e) {
    var formGroup = e.target.closest('.form-group')
    var list = formGroup.querySelector('.result-autocomplete');
    var active = list.querySelector('ul li.active');
    if (active) {
      var value = active.dataset.value;
      formGroup.querySelector('input').value = value;
      list.innerHTML = '';
      formGroup.querySelector('input').select();
    }
  }

})();
